package co.com.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegisterUtestPage {

    public static final Target BUTTON_JOIN_TODAY = Target.the("Click En el Boton Join Today").located(By.xpath("/html/body/ui-view/unauthenticated-container/div/div/unauthenticated-header/div/div[3]/ul[2]/li[2]/a"));
    public static final Target INPUT_FIRST_NAME = Target.the("Digitamos el primer nombre").located(By.id("firstName"));
    public static final Target INPUT_LAST_NAME = Target.the("Digitamos el segundo nombre").located(By.id("lastName"));
    public static final Target INPUT_EMAIL = Target.the("Digitamos el email").located(By.id("email"));
    public static final Target INPUT_BIRTH_MONTH = Target.the("Seleccionamos el mes de nacimiento").located(By.id("birthMonth"));
    public static final Target INPUT_BIRTH_DAY = Target.the("Seleccionamos el dia de nacimiento").located(By.id("birthDay"));
    public static final Target INPUT_BIRTH_YEAR = Target.the("Seleccionamos el año de nacimiento").located(By.id("birthYear"));
    public static final Target BUTTON_NEXT_1 = Target.the("Click En El Boton Siguiente").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/a"));

    public static final Target INPUT_CITY = Target.the("Digitamos la ciudad").located(By.id("city"));
    public static final Target INPUT_ZIP = Target.the("Digitamos la ciudad").located(By.id("zip"));
    public static final Target DIV_CONTRY = Target.the("Seleccionamos el pais").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/div[1]/span/span[2]"));
    public static final Target INPUT_CONTRY = Target.the("Seleccionamos el pais").located(By.xpath("//body/ui-view[1]/main[1]/section[1]/div[1]/div[2]/div[1]/div[2]/div[1]/form[1]/div[1]/div[3]/div[1]/div[4]/div[2]/div[1]/div[1]/input[1]"));
    public static final Target BUTTON_NEXT_2 = Target.the("Click En El Boton Siguiente").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/form/div[2]/div/a"));

    public static final Target DIV_COMPUTER = Target.the("Seleccionamos el computador").located(By.xpath("//*[@id=\"web-device\"]/div[1]/div[2]/div/div[1]"));
    public static final Target INPUT_COMPUTER = Target.the("Digitamos el computador").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[1]/div[1]/div[2]/div/input[1]"));

    public static final Target DIV_VERSION = Target.the("Seleccionamos la version").located(By.xpath("//*[@id=\"web-device\"]/div[2]/div[2]/div/div[1]"));
    public static final Target INPUT_VERSION = Target.the("Digitamos la version").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[1]/div[2]/div[2]/div/input[1]"));

    public static final Target DIV_LENGUAJE = Target.the("Seleccionamos el lenguaje").located(By.xpath("//*[@id=\"web-device\"]/div[3]/div[2]/div/div[1]"));
    public static final Target INPUT_LENGUAJE = Target.the("Digitamos el lenguaje").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[1]/div[3]/div[2]/div/input[1]"));

    public static final Target DIV_MOBILE = Target.the("Seleccionamos el movil").located(By.xpath("//*[@id=\"mobile-device\"]/div[1]/div[2]/div"));
    public static final Target INPUT_MOBILE = Target.the("Digitamos el movil").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[1]/div[2]/div/input[1]"));

    public static final Target DIV_MODEL = Target.the("Seleccionamos el modelo").located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]/div/div[1]"));
    public static final Target INPUT_MODEL = Target.the("Digitamos el modelo").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/input[1]"));

    public static final Target DIV_MODEL_OS = Target.the("Seleccionamos el os").located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div"));
    public static final Target INPUT_MODEL_OS = Target.the("Digitamos el os").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[3]/div[2]/div/input[1]"));
    public static final Target BUTTON_NEXT_3 = Target.the("Click En El Boton Siguiente").located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[2]/div"));

    public static final Target INPUT_PASSWORD_1 = Target.the("Digitamos el password").located(By.xpath("//*[@id=\"password\"]"));
    public static final Target INPUT_PASSWORD_2 = Target.the("Verificamos el password").located(By.xpath("//*[@id=\"confirmPassword\"]"));

    public static final Target INPUT_TERMS_USE = Target.the("Click en terminos de uso").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));
    public static final Target INPUT_PRIVACY_SETTING = Target.the("Click ").located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));


    public static final Target BUTTON_NEXT_4 = Target.the("Click En El Boton Siguiente").located(By.id("laddaBtn"));
    public static final Target BUTTON_NEXT_LABEL= Target.the("Click En El Boton Siguiente").located(By.xpath("//*[@id=\"laddaBtn\"]/span"));
}
