package co.com.utest.questions;

import co.com.utest.userinterface.RegisterUtestPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {
    private String strExpected;

    public Answer(String strExpected) {
        this.strExpected = strExpected;
    }

    public static Answer es(String strExpected) {
        return new Answer(strExpected);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String strNombreBottun = Text.of(RegisterUtestPage.BUTTON_NEXT_LABEL).viewedBy(actor).asString();
        return strExpected.equalsIgnoreCase(strNombreBottun);
    }
}