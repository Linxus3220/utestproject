package co.com.utest.model;

public class UtestData {
    private String strFirtsName;
    private String strLastName;
    private String strEmail;
    private String strMes;
    private String strDia;
    private String strAno;
    private String strCity;
    private String strZip;
    private String strContry;
    private String strComputer;
    private String strVersion;
    private String strLenguaje;
    private String strMovil;
    private String strModelo;
    private String strOs;
    private String strPassword;
    private String strConfirPassword;

    public String getStrFirtsName() {
        return strFirtsName;
    }

    public void setStrFirtsName(String strFirtsName) {
        this.strFirtsName = strFirtsName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrMes() {
        return strMes;
    }

    public void setStrMes(String strMes) {
        this.strMes = strMes;
    }

    public String getStrDia() {
        return strDia;
    }

    public void setStrDia(String strDia) {
        this.strDia = strDia;
    }

    public String getStrAno() {
        return strAno;
    }

    public void setStrAno(String strAno) {
        this.strAno = strAno;
    }

    public String getStrCity() {
        return strCity;
    }

    public void setStrCity(String strCity) {
        this.strCity = strCity;
    }

    public String getStrZip() {
        return strZip;
    }

    public void setStrZip(String strZip) {
        this.strZip = strZip;
    }

    public String getStrContry() {
        return strContry;
    }

    public void setStrContry(String strContry) {
        this.strContry = strContry;
    }

    public String getStrComputer() {
        return strComputer;
    }

    public void setStrComputer(String strComputer) {
        this.strComputer = strComputer;
    }

    public String getStrVersion() {
        return strVersion;
    }

    public void setStrVersion(String strVersion) {
        this.strVersion = strVersion;
    }

    public String getStrLenguaje() {
        return strLenguaje;
    }

    public void setStrLenguaje(String strLenguaje) {
        this.strLenguaje = strLenguaje;
    }

    public String getStrMovil() {
        return strMovil;
    }

    public void setStrMovil(String strMovil) {
        this.strMovil = strMovil;
    }

    public String getStrModelo() {
        return strModelo;
    }

    public void setStrModelo(String strModelo) {
        this.strModelo = strModelo;
    }

    public String getStrOs() {
        return strOs;
    }

    public void setStrOs(String strOs) {
        this.strOs = strOs;
    }

    public String getStrPassword() {
        return strPassword;
    }

    public void setStrPassword(String strPassword) {
        this.strPassword = strPassword;
    }

    public String getStrConfirPassword() {
        return strConfirPassword;
    }

    public void setStrConfirPassword(String strConfirPassword) {
        this.strConfirPassword = strConfirPassword;
    }

}
