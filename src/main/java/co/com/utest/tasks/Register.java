package co.com.utest.tasks;

import co.com.utest.userinterface.RegisterUtestPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;


public class Register implements Task {

    private String strFirtsName;
    private String strLastName;
    private String strEmail;
    private String strMes;
    private String strDia;
    private String strAno;
    private String strCity;
    private String strZip;
    private String strContry;
    private String strComputer;
    private String strVersion;
    private String strLenguaje;
    private String strMovil;
    private String strModelo;
    private String strOs;
    private String strPassword;
    private String strConfirPassword;

    public Register(String strFirtsName, String strLastName, String strEmail, String strMes, String strDia, String strAno, String strCity, String strZip,
                    String strContry, String strComputer, String strVersion, String strLenguaje, String strMovil, String strModelo, String strOs, String strPassword, String strConfirPassword) {
        this.strFirtsName = strFirtsName;
        this.strLastName = strLastName;
        this.strEmail = strEmail;
        this.strMes = strMes;
        this.strDia = strDia;
        this.strAno = strAno;
        this.strCity = strCity;
        this.strZip = strZip;
        this.strContry = strContry;
        this.strComputer = strComputer;
        this.strVersion = strVersion;
        this.strLenguaje = strLenguaje;
        this.strMovil = strMovil;
        this.strModelo = strModelo;
        this.strOs = strOs;
        this.strPassword = strPassword;
        this.strConfirPassword = strConfirPassword;
    }

    public static Register onThePage(String strFirtsName, String strLastName, String strEmail, String strMes, String strDia, String strAno, String strCity, String strZip,
                                     String strContry, String strComputer, String strVersion, String strLenguaje, String strMovil, String strModelo, String strOs, String strPassword, String strConfirPassword) {
        return Tasks.instrumented(Register.class, strFirtsName,  strLastName,  strEmail,  strMes,  strDia,  strAno,  strCity,  strZip,
                 strContry,  strComputer,  strVersion,  strLenguaje,  strMovil,  strModelo,  strOs,  strPassword,  strConfirPassword);
    }
    @Override
    public <T extends Actor> void performAs(T actor){
        actor.attemptsTo(Click.on(RegisterUtestPage.BUTTON_JOIN_TODAY),
                Enter.theValue(strFirtsName).into(RegisterUtestPage.INPUT_FIRST_NAME),
                Enter.theValue(strLastName).into(RegisterUtestPage.INPUT_LAST_NAME),
                Enter.theValue(strEmail).into(RegisterUtestPage.INPUT_EMAIL),
                SelectFromOptions.byVisibleText(strMes).from(RegisterUtestPage.INPUT_BIRTH_MONTH),
                SelectFromOptions.byVisibleText(strDia).from(RegisterUtestPage.INPUT_BIRTH_DAY),
                SelectFromOptions.byVisibleText(strAno).from(RegisterUtestPage.INPUT_BIRTH_YEAR),
                Click.on(RegisterUtestPage.BUTTON_NEXT_1),

                Enter.theValue(strCity).into(RegisterUtestPage.INPUT_CITY),
                Enter.theValue(strZip).into(RegisterUtestPage.INPUT_ZIP),
                Click.on(RegisterUtestPage.DIV_CONTRY),
                WaitUntil.the(RegisterUtestPage.INPUT_CONTRY, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strContry).into(RegisterUtestPage.INPUT_CONTRY).thenHit(Keys.ARROW_UP,Keys.ENTER),
                Click.on(RegisterUtestPage.BUTTON_NEXT_2),

                Click.on(RegisterUtestPage.DIV_COMPUTER),
                WaitUntil.the(RegisterUtestPage.INPUT_COMPUTER, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strComputer).into(RegisterUtestPage.INPUT_COMPUTER).thenHit(Keys.ARROW_UP,Keys.ENTER),

                Click.on(RegisterUtestPage.DIV_VERSION),
                WaitUntil.the(RegisterUtestPage.INPUT_VERSION, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strVersion).into(RegisterUtestPage.INPUT_VERSION).thenHit(Keys.ARROW_UP,Keys.ENTER),

                Click.on(RegisterUtestPage.DIV_LENGUAJE),
                WaitUntil.the(RegisterUtestPage.INPUT_LENGUAJE, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strLenguaje).into(RegisterUtestPage.INPUT_LENGUAJE).thenHit(Keys.ARROW_UP,Keys.ENTER),

                Click.on(RegisterUtestPage.DIV_MOBILE),
                WaitUntil.the(RegisterUtestPage.INPUT_MOBILE, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strMovil).into(RegisterUtestPage.INPUT_MOBILE).thenHit(Keys.ARROW_UP,Keys.ENTER),

                Click.on(RegisterUtestPage.DIV_MODEL),
                WaitUntil.the(RegisterUtestPage.INPUT_MODEL, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strModelo).into(RegisterUtestPage.INPUT_MODEL).thenHit(Keys.ARROW_UP,Keys.ENTER),

                Click.on(RegisterUtestPage.DIV_MODEL_OS),
                WaitUntil.the(RegisterUtestPage.INPUT_MODEL_OS, isVisible()).forNoMoreThan(4).seconds(),
                Enter.theValue(strOs).into(RegisterUtestPage.INPUT_MODEL_OS).thenHit(Keys.ARROW_UP,Keys.ENTER),
                Click.on(RegisterUtestPage.BUTTON_NEXT_3),

                Enter.theValue(strPassword).into(RegisterUtestPage.INPUT_PASSWORD_1),
                Enter.theValue(strConfirPassword).into(RegisterUtestPage.INPUT_PASSWORD_2),

                Click.on(RegisterUtestPage.INPUT_TERMS_USE),
                Click.on(RegisterUtestPage.INPUT_PRIVACY_SETTING),
                Click.on(RegisterUtestPage.BUTTON_NEXT_4)

                );
    }
}