package co.com.utest.stepsdefinitions;

import co.com.utest.model.UtestData;
import co.com.utest.questions.Answer;
import co.com.utest.tasks.OpenUp;
import co.com.utest.tasks.Register;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class UtestStepDefinitions {
    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^moises quiere registrarse en la pagina de utest$")
    public void moisesQuiereRegistrarseEnLaPaginaDeUtest() {
        OnStage.theActorCalled("Moises").wasAbleTo(OpenUp.thePage());
    }

    @When("^el digita todos los datos necesarios para el registro$")
    public void elDigitaTodosLosDatosNecesariosParaElRegistro(List <UtestData> utestData) {
        theActorInTheSpotlight().attemptsTo(Register.onThePage(utestData.get(0).getStrFirtsName(),utestData.get(0).getStrLastName(),utestData.get(0).getStrEmail(),utestData.get(0).getStrMes(),
                utestData.get(0).getStrDia(),utestData.get(0).getStrAno(),utestData.get(0).getStrCity(),utestData.get(0).getStrZip(),utestData.get(0).getStrContry(),utestData.get(0).getStrComputer(),
                utestData.get(0).getStrVersion(),utestData.get(0).getStrLenguaje(),utestData.get(0).getStrMovil(),utestData.get(0).getStrModelo(),utestData.get(0).getStrOs(),utestData.get(0).getStrPassword(),
                utestData.get(0).getStrConfirPassword()));
    }

    @Then("^el ve el boton Complete Setup$")
    public void elVeElBotonCompleteSetup() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.es("Complete Setup")));
    }
}
