package co.com.utest.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (
        features = "src/test/resources/feature/utest.feature",
        tags = "@stories",
        glue = "co.com.utest.stepsdefinitions",
        snippets = SnippetType.CAMELCASE)

public class RunnerTags {
}

