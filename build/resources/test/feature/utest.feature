# Autor: Miguel Alvarez Ojeda
@stories
Feature: Utest
  Un usuario quiere registrarse en la pagina de Utest
  @Scenario1
  Scenario: Registrarse en la pagina de Utest
    Given moises quiere registrarse en la pagina de utest
    When el digita todos los datos necesarios para el registro
      | strFirtsName | strLastName | strEmail        | strMes        | strDia | strAno | strCity   | strZip | strContry  | strComputer   | strVersion   | strLenguaje | strMovil | strModelo | strOs    | strPassword     | strConfirPassword  |
      | Miguel       | Alvarez     | aaaaa@gmail.com | February      | 22     | 1990   | Sincelejo | 700001 | Colombia   | macOS         | Big Sur 11.1 | Spanish     | Apple    | iPhone X  | iOS 14.3 | kVDDWK5%#GtT8?g | kVDDWK5%#GtT8?g    |
    Then el ve el boton Complete Setup
